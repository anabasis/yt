/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package yt

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/tidwall/gjson"
)

// Param is a representation of the parameters supplied for specifying search
// types.
type Param string

// SearchParam and MusicParam specify search type for YouTube and YouTube
// Music, respectively.
type (
	SearchParam Param
	MusicParam  Param
)

// SearchQuery is the data sent to the YouTube search endpoint
// (https://www.youtube.com/youtubei/v1/search) after being JSON-encoded.
type SearchQuery struct {
	Context QueryContext `json:"context"`
	Query   string       `json:"query"`
	Param   Param        `json:"params"`
}

// SearchResults is a representation of search results returned from YouTube
// search.
type SearchResults struct {
	Videos    *[]SimpleVideoSearchResult
	Playlists *[]SimplePlaylist
	Channels  *[]SimpleChannel
}

// MusicSearchResults is a representation of search results returned from
// YouTube Music search.
type MusicSearchResults struct {
	Songs              *[]SimpleVideo
	Videos             *[]SimpleVideo
	Albums             *[]SimpleAlbumPlaylist
	Artists            *[]SimpleArtistChannel
	FeaturedPlaylists  *[]SimplePlaylist
	CommunityPlaylists *[]SimplePlaylist
}

// Search parameters are used for specifying types for YouTube search.
const (
	SearchParamVideo    SearchParam = "EgIQAQ=="
	SearchParamChannel  SearchParam = "EgIQAg=="
	SearchParamPlaylist SearchParam = "EgIQAw=="
	SearchParamMovie    SearchParam = "EgIQBA=="
)

// Music search parameters are used for specifying types for YouTube Music search.
const (
	MusicParamSongs              MusicParam = "EgWKAQIIAWoMEAMQBBAOEAoQCRAF"
	MusicParamVideos             MusicParam = "EgWKAQIQAWoMEAMQBBAOEAoQCRAF"
	MusicParamAlbums             MusicParam = "EgWKAQIYAWoMEAMQBBAOEAoQCRAF"
	MusicParamArtists            MusicParam = "EgWKAQIgAWoMEAMQBBAOEAoQCRAF"
	MusicParamFeaturedPlaylists  MusicParam = "EgeKAQQoADgBagwQAxAEEA4QChAJEAU="
	MusicParamCommunityPlaylists MusicParam = "EgeKAQQoAEABagwQAxAEEA4QChAJEAU="
)

// GetSearchResults returns search results for a YouTube search with the
// supplied query, search types, and language.
func GetSearchResults(query string, param SearchParam, lang string) (SearchResults, error) {
	body, err := sendSearchRequest(query, false, Param(param), lang)
	if err != nil {
		return SearchResults{}, err
	}

	data := gjson.ParseBytes(body)

	results := data.Get(
		`contents.twoColumnSearchResultsRenderer.primaryContents.sectionListRenderer.contents|@reverse.1.itemSectionRenderer.contents`,
	)

	switch param {
	case SearchParamVideo:
		videoResults := getSearchResultsForVideo(results.Get("#.videoRenderer"))
		return SearchResults{Videos: &videoResults}, nil
	case SearchParamPlaylist:
		playlistResults := getSearchResultsForPlaylist(results.Get("#.playlistRenderer"))
		return SearchResults{Playlists: &playlistResults}, nil
	case SearchParamChannel:
		channelResults := getSearchResultsForChannel(results.Get("#.channelRenderer"))
		return SearchResults{Channels: &channelResults}, nil
	}

	return SearchResults{}, nil
}

// getSearchResultsForVideo returns a slice of SimpleVideoSearchResult structs
// from the supplied gjson.Result struct.
func getSearchResultsForVideo(results gjson.Result) []SimpleVideoSearchResult {
	videoResults := []SimpleVideoSearchResult{}

	for _, result := range results.Array() {
		shortDescription := strings.Builder{}
		for _, i := range result.Get("detailedMetadataSnippets.0.snippetText.runs.#.text|@flatten").Array() {
			shortDescription.WriteString(i.Str)
		}

		videoResults = append(videoResults, SimpleVideoSearchResult{
			SimpleVideo: SimpleVideo{
				ID:       result.Get("videoId").Str,
				Title:    result.Get("title.runs.0.text").Str,
				Duration: fmtDurationSeconds(result.Get("lengthText.simpleText").Str),

				ChannelID:   result.Get("ownerText.runs.0.navigationEndpoint.browseEndpoint.browseId").Str,
				ChannelName: result.Get("ownerText.runs.0.text").Str,

				Thumbnails: thumbnailSlice(result.Get("thumbnail.thumbnails").Array()),
			},

			ShortDescription:    shortDescription.String(),
			RelativePublishDate: result.Get("publishedTimeText.simpleText").Str,
			Views:               getViewsInt(result.Get("viewCountText.simpleText")),
		})
	}

	return videoResults
}

// getSearchResultsForPlaylist returns a slice of SimplePlaylist structs from
// the supplied gjson.Result struct.
func getSearchResultsForPlaylist(results gjson.Result) []SimplePlaylist {
	playlistResults := []SimplePlaylist{}

	for _, result := range results.Array() {
		playlistResults = append(playlistResults, SimplePlaylist{
			ID:          result.Get("playlistId").Str,
			Title:       result.Get("title.simpleText").Str,
			VideoNumber: int(result.Get("videoCount").Int()),

			CreatorChannelID:   result.Get("shortBylineText.runs.0.navigationEndpoint.browseEndpoint.browseId").Str,
			CreatorChannelName: result.Get("shortBylineText.runs.0.text").Str,

			Thumbnails: thumbnailSlice(result.Get("thumbnails.0.thumbnails").Array()),
		})
	}

	return playlistResults
}

// getSearchResultsForPlaylist returns a slice of SimpleChannel structs from
// the supplied gjson.Result struct.
func getSearchResultsForChannel(results gjson.Result) []SimpleChannel {
	channelResults := []SimpleChannel{}

	for _, result := range results.Array() {
		avatars := thumbnailSlice(result.Get("thumbnail.thumbnails").Array())
		for i := 0; i < len(avatars); i++ {
			avatars[i].URL = "https:" + avatars[i].URL
		}

		channelResults = append(channelResults, SimpleChannel{
			ID:       result.Get("channelId").Str,
			Name:     result.Get("title.simpleText").Str,
			Username: result.Get("subscriberCountText.simpleText").Str,

			Avatars: avatars,

			ShortDescription: result.Get("descriptionSnippet.runs.0.text").Str,
			Subscribers:      getSubscribersInt(result.Get("videoCountText.simpleText")),
			IsVerified:       result.Get("ownerBadges.0.metadataBadgeRenderer.tooltip").Str == "Verified",
		})
	}

	return channelResults
}

// GetMusicSearchResults returns search results for a YouTube Music search with
// the supplied query, search types, and language.
func GetMusicSearchResults(query string, param MusicParam, lang string) (MusicSearchResults, error) {
	body, err := sendSearchRequest(query, true, Param(param), lang)
	if err != nil {
		return MusicSearchResults{}, err
	}

	data := gjson.ParseBytes(body)

	results := data.Get(
		`contents.tabbedSearchResultsRenderer.tabs.0.tabRenderer.content.sectionListRenderer.contents.0`,
	)

	if results.Get("itemSectionRenderer.contents.0.showingResultsForRenderer").Exists() {
		results = data.Get(
			`contents.tabbedSearchResultsRenderer.tabs.0.tabRenderer.content.sectionListRenderer.contents.1`,
		)
	}

	path := "musicShelfRenderer.contents.#.musicResponsiveListItemRenderer"

	switch param {
	case MusicParamSongs:
		songResults := getMusicResultsForVideo(results.Get(path))
		return MusicSearchResults{Songs: &songResults}, nil
	case MusicParamVideos:
		videoResults := getMusicResultsForVideo(results.Get(path))
		return MusicSearchResults{Videos: &videoResults}, nil
	case MusicParamAlbums:
		albumResults := getMusicResultsForAlbum(results.Get(path))
		return MusicSearchResults{Albums: &albumResults}, nil
	case MusicParamArtists:
		artistResults := getMusicResultsForArtist(results.Get(path))
		return MusicSearchResults{Artists: &artistResults}, nil
	case MusicParamFeaturedPlaylists:
		playlistResults := getMusicResultsForPlaylist(results.Get(path))
		return MusicSearchResults{FeaturedPlaylists: &playlistResults}, nil
	case MusicParamCommunityPlaylists:
		playlistResults := getMusicResultsForPlaylist(results.Get(path))
		return MusicSearchResults{CommunityPlaylists: &playlistResults}, nil
	}

	return MusicSearchResults{}, nil
}

// getSearchResultsForPlaylist returns a slice of SimpleVideo structs from the
// supplied gjson.Result struct.
func getMusicResultsForVideo(results gjson.Result) []SimpleVideo {
	videoResults := []SimpleVideo{}

	for _, result := range results.Array() {
		videoResults = append(videoResults, SimpleVideo{
			ID:    result.Get("playlistItemData.videoId").Str,
			Title: result.Get("flexColumns.0.musicResponsiveListItemFlexColumnRenderer.text.runs.0.text").Str,
			Duration: fmtDurationSeconds(
				result.Get("flexColumns.1.musicResponsiveListItemFlexColumnRenderer.text.runs|@reverse|0.text").Str,
			),

			ChannelID: result.Get(
				"flexColumns.1.musicResponsiveListItemFlexColumnRenderer.text.runs.0.navigationEndpoint.browseEndpoint.browseId",
			).Str,
			ChannelName: result.Get("flexColumns.1.musicResponsiveListItemFlexColumnRenderer.text.runs.0.text").Str,

			Thumbnails: thumbnailSlice(result.Get("thumbnail.musicThumbnailRenderer.thumbnail.thumbnails").Array()),
		})
	}

	return videoResults
}

// getMusicResultsForPlaylist returns a slice of SimplePlaylist structs from
// the supplied gjson.Result struct.
func getMusicResultsForPlaylist(results gjson.Result) []SimplePlaylist {
	playlistResults := []SimplePlaylist{}

	for _, result := range results.Array() {
		playlistResults = append(playlistResults, SimplePlaylist{
			ID:    strings.TrimPrefix(result.Get("navigationEndpoint.browseEndpoint.browseId").Str, "VL"),
			Title: result.Get("flexColumns.0.musicResponsiveListItemFlexColumnRenderer.text.runs.0.text").Str,
			VideoNumber: getInt(
				result.Get("flexColumns.1.musicResponsiveListItemFlexColumnRenderer.text.runs|@reverse|0.text"),
			),

			CreatorChannelID: result.Get(
				"flexColumns.1.musicResponsiveListItemFlexColumnRenderer.text.runs.0.navigationEndpoint.browseEndpoint.browseId",
			).Str,
			CreatorChannelName: result.Get("flexColumns.1.musicResponsiveListItemFlexColumnRenderer.text.runs.0.text").Str,

			Thumbnails: thumbnailSlice(result.Get("thumbnail.musicThumbnailRenderer.thumbnail.thumbnails").Array()),
		})
	}

	return playlistResults
}

// getMusicResultsForAlbum returns a slice of SimpleAlbumPlaylist structs from
// the supplied gjson.Result struct.
func getMusicResultsForAlbum(results gjson.Result) []SimpleAlbumPlaylist {
	playlistResults := []SimpleAlbumPlaylist{}

	for _, result := range results.Array() {
		playlistResults = append(playlistResults, SimpleAlbumPlaylist{
			ID: result.Get(
				"menu.menuRenderer.items.0.menuNavigationItemRenderer.navigationEndpoint.watchPlaylistEndpoint.playlistId",
			).Str,
			Title: result.Get("flexColumns.0.musicResponsiveListItemFlexColumnRenderer.text.runs.0.text").Str,

			IsExplicit: result.Get("badges").Exists(),
			ReleaseYear: int(
				result.Get("flexColumns.1.musicResponsiveListItemFlexColumnRenderer.text.runs|@reverse|0.text").Int(),
			),
			AlbumType: result.Get("flexColumns.1.musicResponsiveListItemFlexColumnRenderer.text.runs.0.text").Str,

			ArtistChannelID: result.Get(
				"flexColumns.1.musicResponsiveListItemFlexColumnRenderer.text.runs.2.navigationEndpoint.browseEndpoint.browseId",
			).Str,
			ArtistChannelName: result.Get("flexColumns.1.musicResponsiveListItemFlexColumnRenderer.text.runs.2.text").Str,

			Thumbnails: thumbnailSlice(result.Get("thumbnail.musicThumbnailRenderer.thumbnail.thumbnails").Array()),
		})
	}

	return playlistResults
}

// getMusicResultsForAlbum returns a slice of SimpleArtistChannel structs from
// the supplied gjson.Result struct.
func getMusicResultsForArtist(results gjson.Result) []SimpleArtistChannel {
	artistResults := []SimpleArtistChannel{}

	for _, result := range results.Array() {
		artistResults = append(artistResults, SimpleArtistChannel{
			ID:   result.Get("navigationEndpoint.browseEndpoint.browseId").Str,
			Name: result.Get("flexColumns.0.musicResponsiveListItemFlexColumnRenderer.text.runs.0.text").Str,
			Subscribers: getSubscribersInt(
				result.Get("flexColumns.1.musicResponsiveListItemFlexColumnRenderer.text.runs|@reverse.0.text"),
			),

			Thumbnails: thumbnailSlice(result.Get("thumbnail.musicThumbnailRenderer.thumbnail.thumbnails").Array()),
		})
	}

	return artistResults
}

// sendSearchRequest sends a request to the YouTube search endpoint
// (https://www.youtube.com/youtubei/v1/search) with the supplied query,
// parameter, and whether the search is for YouTube or YouTube Music and
// returns the body of the response and any errors encountered.
func sendSearchRequest(searchQuery string, searchMusic bool, param Param, lang string) ([]byte, error) {
	client := &http.Client{}

	query, err := constructSearchQuery(searchQuery, searchMusic, param, lang)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(
		http.MethodPost,
		"https://www.youtube.com/youtubei/v1/search?prettyPrint=false",
		bytes.NewReader(query),
	)
	if err != nil {
		return nil, fmt.Errorf("failed at constructing request: %w", err)
	}

	req.Header.Set("User-Agent", webUserAgent)

	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed at performing search request: %w", err)
	}

	if resp.StatusCode != 200 {
		return nil, errStatusCodeNotOK
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed at reading response of search request: %w", err)
	}

	return body, nil
}

// constructSearchQuery returns a JSON-encoded query for the YouTube search
// endpoint (https://www.youtube.com/youtubei/v1/search) with the supplied
// query, parameter, and whether the search is for YouTube or YouTube Music as
// well as any errors encountered.
func constructSearchQuery(searchQuery string, searchMusic bool, param Param, lang string) ([]byte, error) {
	constructedQuery := SearchQuery{
		Query: searchQuery,
		Context: QueryContext{
			ClientContext{
				ClientName:    "WEB",
				ClientVersion: webClientVersion,
				Language:      lang,
			},
		},
		Param: param,
	}

	if searchMusic {
		constructedQuery.Context = QueryContext{
			ClientContext{
				ClientName:    "WEB_REMIX",
				ClientVersion: webRemixClientVersion,
				Language:      lang,
			},
		}
	}

	query, err := json.Marshal(constructedQuery)
	if err != nil {
		return nil, fmt.Errorf("failed at encoding JSON value of search query: %w", err)
	}

	return query, nil
}

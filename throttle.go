/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package yt

import (
	"fmt"
	"net/url"
	"regexp"

	"github.com/robertkrimen/otto"
)

// GetDethrottleSignature parses a stream URL like
// `https://rr4---sn-5uaeznld.googlevideo.com/videoplayback?...&n=yYOTSOeAS-63NqMqba&...`
// and returns the dethrottled value for the `n` key of the URL like
// `_SM0t1LByqSOuQ` (from `yYOTSOeAS-63NqMqba`) and any errors encountered.
func GetDethrottleSignature(streamURL string) (string, error) {
	parsed, err := url.Parse(streamURL)
	if err != nil {
		return "", fmt.Errorf("failed at parsing URL: %w", err)
	}

	q := parsed.Query()

	if !q.Has("n") {
		return "", fmt.Errorf("%w: throttle parameter not found", errCannotDethrottle)
	}

	function, err := getDethrottleFunctionFromCache()
	if err != nil {
		return "", err
	}

	if vm == nil {
		vm = otto.New()
	}

	value, err := vm.Call("("+function+")", nil, q.Get("n"))
	if err != nil {
		return "", fmt.Errorf("failed at calling dethrottling function: %w", err)
	}

	val, err := value.ToString()
	if err != nil {
		return "", fmt.Errorf("failed at converting output value to string: %w", err)
	}

	return val, nil
}

// getDethrottleFunction returns the function for dethrottling streams from the
// YouTube base JavaScript file.
func getDethrottleFunction() (string, error) {
	var err error

	if jsFile == "" {
		jsFile, err = GetBaseJS()
		if err != nil {
			return "", err
		}
	}

	throttleFuncNameRegex := regexp.MustCompile(
		`\.set\("n",[A-Za-z$_][0-9A-Za-z$_]*\),[A-Za-z$_][0-9A-Za-z$_]{0,2}\.length\|\|([A-Za-z$_][0-9A-Za-z$_]{0,2})`,
	)
	matches := throttleFuncNameRegex.FindStringSubmatch(jsFile)

	if len(matches) != 2 {
		return "", fmt.Errorf("%w: dethrottling function not found", errCannotDethrottle)
	}

	throttleFuncRegex := regexp.MustCompile(`(?s)` + regexp.QuoteMeta(matches[1]) + `=(function\(a\)\{.+?\});`)
	matches = throttleFuncRegex.FindStringSubmatch(jsFile)

	if len(matches) != 2 {
		return "", fmt.Errorf("%w: dethrottling function not found", errCannotDethrottle)
	}

	return matches[1], nil
}

// getDethrottleFunctionFromCache returns the function for dethrottling streams
// by attempting to retrieve it from the cache file, or if the function for the
// current player version does not exists, it calls getDethrottleFunction.
func getDethrottleFunctionFromCache() (string, error) {
	if playerThrottle != "" {
		return playerThrottle, nil
	}

	var err error

	playerThrottle, err = getAttrFromCache(playerThrottle, "throttle", getDethrottleFunction)
	if err != nil {
		return playerThrottle, err
	}

	return playerThrottle, nil
}

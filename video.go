/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package yt

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/tidwall/gjson"
)

// SimpleVideo is a representation of the simplified data retrieved for videos
// on YouTube on channel pages and some search results.
type SimpleVideo struct {
	ID       string
	Title    string
	Duration time.Duration

	ChannelID   string
	ChannelName string

	Thumbnails []Thumbnail
}

// SimpleVideoSearchResult is a representation of the data retrieved from
// search results for a video on YouTube.
type SimpleVideoSearchResult struct {
	SimpleVideo

	ShortDescription    string
	RelativePublishDate string
	Views               int
}

// Video is a representation of the more complete data for a video retrieved
// from the video page on YouTube.
type Video struct {
	SimpleVideo

	Views       int
	UploadDate  time.Time
	PublishDate time.Time

	Description string
	Tags        []string
	Category    string

	AvailableCountries []string
	RatingsAllowed     bool
	IsCrawlable        bool
	IsLive             bool
	IsPrivate          bool
	IsRestricted       bool
	IsUnlisted         bool

	StreamingData StreamingFormats
	CaptionData   []Caption
}

// Thumbnail is a representation of the data present for thumbnails.
type Thumbnail struct {
	URL    string
	Width  int
	Height int
}

// StreamingFormats is a representation of the data present for streaming
// formats for a YouTube video.
type StreamingFormats struct {
	StaticFormats   []StreamFormat
	AdaptiveFormats []StreamFormat
}

// StreamFormat is a representation of the data present for a stream format for
// a YouTube video.
type StreamFormat struct {
	ITag     int
	URL      string
	MimeType string
	Bitrate  int
}

type Caption struct {
	Name         string
	LanguageCode string
	BaseURL      string
}

// VideoQuery is the data sent to the YouTube player endpoint
// (https://www.youtube.com/youtubei/v1/player) after being JSON-encoded.
type VideoQuery struct {
	VideoID         string          `json:"videoId"`
	Context         QueryContext    `json:"context"`
	ThirdParty      ThirdPartyEmbed `json:"thirdParty,omitempty"`
	PlaybackContext PlaybackContext `json:"playbackContext,omitempty"`
}

// QueryContext is a struct for wrapping the ClientContext struct for video
// queries.
type QueryContext struct {
	Client ClientContext `json:"client"`
}

// ClientContext is a struct for wrapping client information for video queries.
type ClientContext struct {
	ClientName    string `json:"clientName"`
	ClientVersion string `json:"clientVersion"`
	SDKVersion    int    `json:"androidSdkVersion,omitempty"`
	Language      string `json:"hl,omitempty"`
}

// ThirdPartyEmbed is a struct for wrapping the embed URL for video queries.
type ThirdPartyEmbed struct {
	EmbedURL string `json:"embedUrl"`
}

type PlaybackContext struct {
	ContentPlaybackContext ContentPlaybackContext `json:"contentPlaybackContext"`
}

type ContentPlaybackContext struct {
	SignatureTimestamp int `json:"signatureTimestamp"`
}

// GetVideo returns data for a video ID as Video struct with any errors
// encountered.
func GetVideo(id string) (Video, error) {
	body, err := sendVideoRequest(id, false)
	if err != nil {
		return Video{}, err
	}

	data := gjson.ParseBytes(body)
	if data.Get("playabilityStatus.status").Str != "OK" {
		body, err = sendVideoRequest(id, true)
		if err != nil {
			return Video{}, err
		}

		data = gjson.ParseBytes(body)

		if data.Get("playabilityStatus.status").Str != "OK" {
			return Video{}, fmt.Errorf("%w: %s", errUnplayableVideo, data.Get("playabilityStatus.reason").Str)
		}
	}

	details := data.Get("videoDetails")
	microFormat := data.Get("microformat.playerMicroformatRenderer")
	streamingData := data.Get("streamingData")

	formats := StreamingFormats{}

	formats.StaticFormats, err = getFormats(streamingData.Get("formats").Array())
	if err != nil {
		return Video{}, err
	}

	formats.AdaptiveFormats, err = getFormats(streamingData.Get("adaptiveFormats").Array())
	if err != nil {
		return Video{}, err
	}

	uploadDate, _ := time.Parse(time.DateOnly, microFormat.Get("uploadDate").Str)
	publishDate, _ := time.Parse(time.DateOnly, microFormat.Get("publishDate").Str)

	return Video{
		SimpleVideo: SimpleVideo{
			ID:       details.Get("videoId").Str,
			Title:    details.Get("title").Str,
			Duration: time.Duration(details.Get("lengthSeconds").Int() * 1e9),

			ChannelID:   details.Get("channelId").String(),
			ChannelName: details.Get("author").String(),

			Thumbnails: thumbnailSlice(details.Get("thumbnail.thumbnails").Array()),
		},

		Views:       int(details.Get("viewCount").Int()),
		UploadDate:  uploadDate,
		PublishDate: publishDate,

		Description: details.Get("shortDescription").String(),
		Tags:        stringSlice(details.Get("keywords").Array()),
		Category:    details.Get("category").String(),

		AvailableCountries: stringSlice(microFormat.Get("availableCountries").Array()),
		RatingsAllowed:     details.Get("allowRatings").Bool(),
		IsCrawlable:        details.Get("isCrawlable").Bool(),
		IsLive:             details.Get("isLiveContent").Bool(),
		IsRestricted:       !microFormat.Get("isFamilySafe").Bool(),
		IsPrivate:          details.Get("isPrivate").Bool(),
		IsUnlisted:         microFormat.Get("isUnlisted").Bool(),

		StreamingData: formats,
		CaptionData:   getCaptions(data.Get("captions.playerCaptionsTracklistRenderer.captionTracks").Array()),
	}, nil
}

// sendVideoRequest sends a request to the YouTube player endpoint
// (https://www.youtube.com/youtubei/v1/player) with the supplied ID and
// returns the body of the response and any errors encountered. The function
// must also be supplied with whether the request is being retried for whether
// an alternative client should be tried.
func sendVideoRequest(id string, retry bool) ([]byte, error) {
	client := &http.Client{}

	query, err := constructVideoQuery(id, retry)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(
		http.MethodPost,
		"https://www.youtube.com/youtubei/v1/player?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8&prettyPrint=false",
		bytes.NewReader(query),
	)
	if err != nil {
		return nil, fmt.Errorf("failed at constructing request: %w", err)
	}

	if !retry {
		req.Header.Set("User-Agent", androidUserAgent)
	} else {
		req.Header.Set("User-Agent", webUserAgent)
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed at performing player request: %w", err)
	}

	if resp.StatusCode != 200 {
		return nil, errStatusCodeNotOK
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed at reading response of player request: %w", err)
	}

	return body, nil
}

// constructVideoQuery returns a JSON-encoded query for the YouTube player
// endpoint (https://www.youtube.com/youtubei/v1/player) with the supplied ID
// as well as any errors encountered.
func constructVideoQuery(id string, retry bool) ([]byte, error) {
	constructedQuery := VideoQuery{
		VideoID: id,
		Context: QueryContext{
			ClientContext{
				ClientName:    "ANDROID",
				ClientVersion: androidClientVersion,
				SDKVersion:    androidSDKVersion,
				Language:      "en",
			},
		},
	}

	if retry {
		constructedQuery.Context = QueryContext{
			ClientContext{
				ClientName:    "TVHTML5_SIMPLY_EMBEDDED_PLAYER",
				ClientVersion: tvEmbeddedPlayerClientVersion,
				Language:      "en",
			},
		}
		constructedQuery.ThirdParty = ThirdPartyEmbed{"https://www.youtube.com/"}
	}

	query, err := json.Marshal(constructedQuery)
	if err != nil {
		return nil, fmt.Errorf("failed at encoding JSON value of video query: %w", err)
	}

	return query, nil
}

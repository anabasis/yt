# `yt`

[![Go Reference](https://pkg.go.dev/badge/codeberg.org/anabasis/yt)](https://pkg.go.dev/codeberg.org/anabasis/yt)

`yt` is a Go library for accessing YouTube.
To use it, install by running the following:

```shell
go get codeberg.org/anabasis/yt
```

This repository also contains the `yt-cli` command-line program, which can be found in the [`cmd/`](./cmd) directory.
For more information about `yt-cli`, read [`cmd/README.md`](./cmd/README.md).

## :gear: Features
`yt` is still currently a work-in-progress, but it does implement the following:
- search results for YouTube and YouTube Music
- retrieving data for videos, playlists, and channels
- deciphering URLs for videos with music and deciphering throttling signatures for videos
- viewing videos on the "Trending" list and homepage of YouTube

## :page\_facing\_up: License
This repository, including `yt` and `yt-cli`, is licensed under the Mozilla Public License Version 2.0.
For more details, please see [`LICENSE`](./LICENSE).

## :pray: Acknowledgments
This would not be possible without [yt-dlp](https://github.com/yt-dlp/yt-dlp) and [NewPipeExtractor](https://github.com/TeamNewPipe/NewPipeExtractor).

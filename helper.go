/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package yt

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/robertkrimen/otto"
	"github.com/tidwall/gjson"
)

var (
	cacheDir   string
	playerName string
	jsFile     string

	playerCipher    DecipherFunctionData
	playerThrottle  string
	playerTimestamp int

	vm *otto.Otto
)

var (
	errCannotDethrottle      = errors.New("unable to dethrottle stream")
	errSigCipherFuncNotFound = errors.New("unable to get signature decipher function")
	errStatusCodeNotOK       = errors.New("HTTP request status is not 200")
	errPathNotDir            = errors.New("cache directory path is not a directory")
	errUnplayableVideo       = errors.New("video cannot be played")
	errPlayerNameNotFound    = errors.New("player version cannot be found")
)

const (
	webUserAgent                  = "Mozilla/5.0 (Windows NT 10.0; rv:113.0) Gecko/20100101 Firefox/113.0"
	webClientVersion              = "2.20230515.10.00"
	webRemixClientVersion         = "1.20230515.01.00"
	tvEmbeddedPlayerClientVersion = "2.0"

	androidUserAgent     = "com.google.android.youtube/18.22.35"
	androidClientVersion = "18.22.35"
	androidSDKVersion    = 33
)

// init initializes the library by establishing the cache directory and getting
// the name of the current player version.
func init() {
	dir, err := os.UserCacheDir()
	if err != nil {
		panic(err)
	}

	cacheDir = filepath.Join(dir, "yt")
}

// GetBaseJS gets the base JavaScript file for the current player version.
func GetBaseJS() (string, error) {
	if playerName == "" {
		var err error

		playerName, err = GetPlayerName()
		if err != nil {
			return "", err
		}
	}

	client := &http.Client{}

	req, err := http.NewRequest(
		http.MethodGet,
		"https://www.youtube.com/s/player/"+playerName+"/player_ias.vflset/en_US/base.js",
		nil,
	)
	if err != nil {
		return "", fmt.Errorf("failed at constructing request: %w", err)
	}

	req.Header.Set("User-Agent", webUserAgent)

	resp, err := client.Do(req)
	if err != nil {
		return "", fmt.Errorf("failed at requesting base JS file: %w", err)
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("failed at reading response of base JS file: %w", err)
	}

	return string(body), nil
}

// GetPlayerName gets the name of the current player version.
func GetPlayerName() (string, error) {
	client := &http.Client{}

	req, err := http.NewRequest(http.MethodGet, "https://www.youtube.com/iframe_api", nil)
	if err != nil {
		return "", fmt.Errorf("failed at constructing request: %w", err)
	}

	req.Header.Set("User-Agent", webUserAgent)

	resp, err := client.Do(req)
	if err != nil {
		return "", fmt.Errorf("failed at requesting iFrame API JS file: %w", err)
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("failed at reading response of iFrame API JS file: %w", err)
	}

	matches := regexp.MustCompile(`player\\?/([0-9a-fA-F]{8})\\?/`).FindStringSubmatch(string(body))
	if len(matches) == 2 {
		return matches[1], nil
	}

	return "", errPlayerNameNotFound
}

func GetSignatureTimestamp() (int, error) {
	if playerTimestamp != 0 {
		return playerTimestamp, nil
	}

	var err error

	playerTimestamp, err = getAttrFromCache(playerTimestamp, "timestamp", getSignatureTimestampFromJSFile)
	if err != nil {
		return playerTimestamp, err
	}

	return playerTimestamp, nil
}

func getSignatureTimestampFromJSFile() (int, error) {
	var err error
	if jsFile == "" {
		jsFile, err = GetBaseJS()
		if err != nil {
			return 0, err
		}
	}

	matches := regexp.MustCompile(`signatureTimestamp:(\d+)`).FindStringSubmatch(jsFile)
	if len(matches) != 2 {
		return 0, errors.New("match not found")
	}

	num, err := strconv.Atoi(matches[1])
	if err != nil {
		return 0, err
	}

	return num, nil
}

// SetCacheDir sets the cache directory to the supplied value.
func SetCacheDir(dir string) {
	cacheDir = dir
}

// fmtDurationSeconds converts a string with minutes and seconds like "4:05" to
// an int with the total number of seconds.
func fmtDurationSeconds(duration string) time.Duration {
	seconds := 0
	split := strings.Split(duration, ":")

	x := 1
	for i := len(split) - 1; i >= 0; i-- {
		j, _ := strconv.Atoi(split[i])
		seconds += j * x
		x *= 60
	}

	return time.Duration(seconds * 1e9)
}

// stringSlice converts a slice of gjson.Result structs to a slice of strings.
func stringSlice(r []gjson.Result) []string {
	s := []string{}
	for _, x := range r {
		s = append(s, x.Str)
	}

	return s
}

// stringSlice converts a slice of gjson.Result structs to a slice of Thumbnail
// structs.
func thumbnailSlice(r []gjson.Result) []Thumbnail {
	thumbnails := []Thumbnail{}
	for _, x := range r {
		thumbnails = append(thumbnails, Thumbnail{
			x.Get("url").Str,
			int(x.Get("width").Int()),
			int(x.Get("height").Int()),
		})
	}

	return thumbnails
}

// getInt returns an int from a gjson.Result struct with a string with trailing
// text.
func getInt(r gjson.Result) int {
	num, _ := strconv.Atoi(strings.Split(r.Str, " ")[0])

	return num
}

// getViewsInt returns an int from a gjson.Result struct for a video's view
// count.
func getViewsInt(r gjson.Result) int {
	num, _ := strconv.Atoi(strings.ReplaceAll(strings.TrimSuffix(r.Str, " views"), ",", ""))

	return num
}

// getSubscribersInt returns an int from a gjson.Result struct for a channel's
// subscriber count.
func getSubscribersInt(r gjson.Result) int {
	subs := strings.TrimSuffix(r.Str, " subscribers")
	if subs == "" {
		return 0
	}

	mult := 1

	var n float64

	switch subs[len(subs)-1] {
	case 'K':
		mult = 1000
		n, _ = strconv.ParseFloat(subs[:len(subs)-1], 64)
	case 'M':
		mult = 1000000
		n, _ = strconv.ParseFloat(subs[:len(subs)-1], 64)
	default:
		n, _ = strconv.ParseFloat(subs, 64)
	}

	return int(n) * mult
}

// getUpdateDate returns a time.Time struct from a gjson.Result struct for when
// a video was updated/released.
func getUpdateDate(r gjson.Result) time.Time {
	s := r.Str
	now := time.Now()
	midnight := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())

	switch {
	case s == "today":
		return midnight
	case s == "yesterday":
		return midnight.Add(-24 * time.Hour)
	case strings.HasSuffix(s, " days ago"):
		num, _ := strconv.Atoi(strings.TrimSuffix(s, " days ago"))

		return midnight.Add(time.Duration(num) * -24 * time.Hour)
	}

	date, _ := time.Parse("Jan 2, 2006", r.Str)

	return date
}

// simpleVideoParser appends to a slice of SimpleVideo structs supplied from a
// slice of gjson.Result structs for video results.
func simpleVideoParser(videos []SimpleVideo, r []gjson.Result) []SimpleVideo {
	for _, x := range r {
		duration := time.Duration(x.Get("lengthSeconds").Int() * 1e9)
		if duration == 0 {
			duration = fmtDurationSeconds(x.Get("lengthText.simpleText").Str)
		}

		videos = append(videos, SimpleVideo{
			ID:       x.Get("videoId").Str,
			Title:    x.Get("title.runs.0.text").Str,
			Duration: duration,

			ChannelID:   x.Get("shortBylineText.runs.0.navigationEndpoint.browseEndpoint.browseId").Str,
			ChannelName: x.Get("shortBylineText.runs.0.text").Str,

			Thumbnails: thumbnailSlice(x.Get("thumbnail.thumbnails").Array()),
		})
	}

	return videos
}

// getChannelLinks returns a map with keys for link titles and values of link
// URLs from a slice of gjson.Result structs of links from a channel page.
func getChannelLinks(r []gjson.Result) map[string]string {
	links := make(map[string]string)

	for _, x := range r {
		parsed, _ := url.Parse(x.Get("navigationEndpoint.urlEndpoint.url").Str)
		links[x.Get("title.simpleText").Str] = parsed.Query().Get("q")
	}

	return links
}

// getFormats returns a slice of StreamFormat structs parsed from a slice of
// gjson.Result structs for video stream formats.
func getFormats(r []gjson.Result) ([]StreamFormat, error) {
	var err error

	nsig := ""
	formats := []StreamFormat{}

	for _, x := range r {
		streamURL := x.Get("url").Str
		sig := x.Get("signatureCipher")

		if sig.Exists() {
			streamURL, err = DecipherSignature(sig.Str)
			if err != nil {
				return []StreamFormat{}, err
			}
		}

		if nsig == "" {
			nsig, err = GetDethrottleSignature(streamURL)
			if err != nil {
				formats = append(formats, StreamFormat{
					int(x.Get("itag").Int()),
					streamURL,
					x.Get("mimeType").Str,
					int(x.Get("bitrate").Int()),
				})

				continue
			}
		}

		var parsedURL *url.URL

		parsedURL, err = url.Parse(streamURL)
		if err != nil {
			return []StreamFormat{}, fmt.Errorf("failed at parsing URL: %w", err)
		}

		query := parsedURL.Query()
		query.Set("n", nsig)
		parsedURL.RawQuery = query.Encode()

		formats = append(formats, StreamFormat{
			int(x.Get("itag").Int()),
			parsedURL.String(),
			x.Get("mimeType").Str,
			int(x.Get("bitrate").Int()),
		})
	}

	return formats, nil
}

func getCaptions(r []gjson.Result) []Caption {
	captions := []Caption{}

	for _, x := range r {
		base, _ := url.Parse(x.Get("baseUrl").Str)
		query := base.Query()
		query.Del("fmt")

		base.RawQuery = query.Encode()

		captions = append(captions, Caption{x.Get("name.runs.0.text").Str, x.Get("languageCode").Str, base.String()})
	}

	return captions
}

func getAttrFromCache[T DecipherFunctionData | string | int](
	playerAttr T,
	attrName string,
	retrieveFunc func() (T, error),
) (T, error) {
	defaultVal := playerAttr

	var err error

	playerName, err = GetPlayerName()
	if err != nil {
		return playerAttr, err
	}

	dirInfo, err := os.Stat(cacheDir)
	if os.IsNotExist(err) {
		err = os.Mkdir(cacheDir, 0o755)
		if err != nil {
			return playerAttr, fmt.Errorf("failed at creating cache directory: %w", err)
		}
	} else if !dirInfo.IsDir() {
		return playerAttr, errPathNotDir
	}

	cacheFile := filepath.Join(cacheDir, attrName+"-cache.json")

	attrMap := map[string]T{}

	var contents []byte

	if _, err = os.Stat(cacheFile); os.IsNotExist(err) {
		// if file does not exist
		_, err = os.Create(cacheFile)
		if err != nil {
			return playerAttr, fmt.Errorf("failed at creating %s cache file: %w", attrName, err)
		}

		playerAttr, err = retrieveFunc()
		if err != nil {
			return playerAttr, err
		}

		attrMap[playerName] = playerAttr

		contents, err = json.Marshal(&attrMap)
		if err != nil {
			return defaultVal, fmt.Errorf("failed at encoding JSON value of %s cache: %w", attrName, err)
		}

		err = os.WriteFile(cacheFile, contents, 0o600)
		if err != nil {
			return defaultVal, fmt.Errorf("failed at writing to %s cache file: %w", attrName, err)
		}
	} else {
		// if file exists
		contents, err = os.ReadFile(cacheFile)
		if err != nil {
			return defaultVal, fmt.Errorf("failed at reading from %s cache file: %w", attrName, err)
		}

		err = json.Unmarshal(contents, &attrMap)
		if err != nil {
			return defaultVal, fmt.Errorf("failed at decoding JSON value of %s cache: %w", attrName, err)
		}

		playerAttr = attrMap[playerName]

		if playerAttr == defaultVal {
			playerAttr, err = retrieveFunc()
			if err != nil {
				return defaultVal, err
			}

			attrMap[playerName] = playerAttr
			contents, err = json.Marshal(&attrMap)
			if err != nil {
				return defaultVal, fmt.Errorf("failed at encoding JSON value of %s cache: %w", attrName, err)
			}

			err = os.WriteFile(cacheFile, contents, 0o600)
			if err != nil {
				return defaultVal, fmt.Errorf("failed at writing to %s cache file: %w", attrName, err)
			}
		}
	}

	return playerAttr, nil
}

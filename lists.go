/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package yt

import "github.com/tidwall/gjson"

// GetTrendingVideos returns the list of videos that are currently trending
// as a slice of SimpleVideo structs.
func GetTrendingVideos() ([]SimpleVideo, error) {
	body, err := sendBrowseRequest("FEtrending", "")
	if err != nil {
		return []SimpleVideo{}, err
	}

	data := gjson.ParseBytes(body)
	results := data.Get(
		"contents.twoColumnBrowseResultsRenderer.tabs.0.tabRenderer.content.sectionListRenderer.contents.#.itemSectionRenderer.contents.0.shelfRenderer.content.expandedShelfContentsRenderer.items",
	).Array()

	// remove "Recently trending" videos
	results = results[:len(results)-1]

	videos := []SimpleVideo{}
	for _, x := range results {
		videos = simpleVideoParser(videos, x.Get("#.videoRenderer").Array())
	}

	return videos, nil
}

// GetFrontPageVideos returns the list of videos that are on the YouTube
// front page (https://www.youtube.com) as a slice of SimpleVideo structs.
// These videos will be random videos since no user data is saved (no user
// recommendations).
func GetFrontPageVideos() ([]SimpleVideo, error) {
	body, err := sendBrowseRequest("FEwhat_to_watch", "")
	if err != nil {
		return []SimpleVideo{}, err
	}

	data := gjson.ParseBytes(body)
	results := data.Get(
		"contents.twoColumnBrowseResultsRenderer.tabs.0.tabRenderer.content.richGridRenderer.contents.#.richItemRenderer.content.videoRenderer",
	)

	return simpleVideoParser([]SimpleVideo{}, results.Array()), nil
}

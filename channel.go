/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package yt

import "github.com/tidwall/gjson"

// SimpleArtistChannel is a representation of the data retrieved from search
// results for an artist on YouTube Music.
type SimpleArtistChannel struct {
	ID          string
	Name        string
	Subscribers int

	Thumbnails []Thumbnail
}

// SimpleArtistChannel is a representation of data retrieved from search
// results for an artist on YouTube Music.
type SimpleChannel struct {
	ID       string
	Name     string
	Username string

	Avatars []Thumbnail

	ShortDescription string
	Subscribers      int
	IsVerified       bool
}

// Channel is a representation of the more complete data for a channel
// retrieved from the channel page on YouTube.
type Channel struct {
	SimpleChannel

	Links        map[string]string
	Banner       []Thumbnail
	TVBanner     []Thumbnail
	MobileBanner []Thumbnail

	TotalVideos int
	Videos      []SimpleVideo

	IsFamilySafe       bool
	AvailableCountries []string
}

// GetChannel returns data for a channel ID as Channel struct with any errors
// encountered.
func GetChannel(id string) (Channel, error) {
	body, err := sendBrowseRequest(id, "EgZ2aWRlb3PyBgQKAjoA")
	if err != nil {
		return Channel{}, err
	}

	data := gjson.ParseBytes(body)
	microformat := data.Get("microformat.microformatDataRenderer")
	header := data.Get("header.c4TabbedHeaderRenderer")
	videoResults := data.Get(
		"contents.twoColumnBrowseResultsRenderer.tabs.1.tabRenderer.content.richGridRenderer.contents.#.richItemRenderer.content.videoRenderer",
	)

	return Channel{
		SimpleChannel: SimpleChannel{
			ID:       header.Get("channelId").Str,
			Name:     microformat.Get("title").Str,
			Username: header.Get("channelHandleText.runs.0.text").Str,

			Avatars: thumbnailSlice(header.Get("avatar.thumbnails").Array()),

			ShortDescription: microformat.Get("description").Str,
			Subscribers:      getSubscribersInt(header.Get("subscriberCountText.simpleText")),
			IsVerified:       header.Get("badges.0.metadataBadgeRenderer.tooltip").Str == "Verified",
		},

		Links:        getChannelLinks(header.Get("headerLinks.channelHeaderLinksRenderer|@values|@flatten").Array()),
		Banner:       thumbnailSlice(header.Get("banner.thumbnails").Array()),
		TVBanner:     thumbnailSlice(header.Get("tvBsanner.thumbnails").Array()),
		MobileBanner: thumbnailSlice(header.Get("mobileBanner.thumbnails").Array()),

		TotalVideos: int(header.Get("videosCountText.runs.0.text").Int()),
		Videos:      simpleVideoParser([]SimpleVideo{}, videoResults.Array()),

		IsFamilySafe:       microformat.Get("familySafe").Bool(),
		AvailableCountries: stringSlice(microformat.Get("availableCountries").Array()),
	}, nil
}

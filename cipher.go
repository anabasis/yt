/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package yt

import (
	"fmt"
	"net/url"
	"regexp"

	"github.com/robertkrimen/otto"
)

// DecipherFunctionData is the data necessary for deciphering a "signature
// cipher" for a video and is the format for the data is cached.
type DecipherFunctionData struct {
	DecipherFunction string `json:"function"`
	DecipherObject   string `json:"object"`
}

// DecipherSignature parses a "signature cipher" from a video like
// `s=AAOA...&sp=sig&url=https://rr4---sn-5uaeznld.googlevideo.com/videoplayback...`
// and returns a deciphered URL and any errors encountered.
func DecipherSignature(sig string) (string, error) {
	query, err := url.ParseQuery(sig)
	if err != nil {
		return "", fmt.Errorf("failed to parse URL: %w", err)
	}

	cipher, err := getDecipherFunctionFromCache()
	if err != nil {
		return "", err
	}

	if vm == nil {
		vm = otto.New()
	}

	_, err = vm.Run(cipher.DecipherObject)
	if err != nil {
		return "", fmt.Errorf("failed at assigning decipher function helper object: %w", err)
	}

	value, err := vm.Call("("+cipher.DecipherFunction+")", nil, query.Get("s"))
	if err != nil {
		return "", fmt.Errorf("failed at calling decipher function: %w", err)
	}

	sig, err = value.ToString()
	if err != nil {
		return "", fmt.Errorf("failed at converting output value to string: %w", err)
	}

	return query.Get("url") + "&" + query.Get("sp") + "=" + url.QueryEscape(sig), nil
}

// getDecipherFunction returns data for the deciphering function from the
// YouTube base JavaScript file.
func getDecipherFunction() (DecipherFunctionData, error) {
	var err error
	if jsFile == "" {
		jsFile, err = GetBaseJS()
		if err != nil {
			return DecipherFunctionData{}, err
		}
	}

	decipherFuncRegex := regexp.MustCompile(
		`function\(a\)\{a=a.split\(""\);([A-Za-z$_][0-9A-Za-z$_]{0,2}).+?;return a.join\(""\)\}`,
	)
	matches := decipherFuncRegex.FindStringSubmatch(jsFile)

	if len(matches) != 2 {
		return DecipherFunctionData{}, errSigCipherFuncNotFound
	}

	decipherFunc := matches[0]
	decipherObjName := matches[1]
	decipherObj := regexp.MustCompile(`(?s)var ` + regexp.QuoteMeta(decipherObjName) + `=\{.+?\};`).FindString(jsFile)

	return DecipherFunctionData{decipherFunc, decipherObj}, nil
}

// getDecipherFunctionFromCache returns data for the deciphering function by
// attempting to retrieve it from the cache file, or if the data for the
// current player version does not exist, it calls getDecipherFunction.
func getDecipherFunctionFromCache() (DecipherFunctionData, error) {
	if playerCipher != (DecipherFunctionData{}) {
		return playerCipher, nil
	}

	var err error

	playerCipher, err = getAttrFromCache(playerCipher, "signature", getDecipherFunction)
	if err != nil {
		return playerCipher, err
	}

	return playerCipher, nil
}

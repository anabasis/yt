package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"codeberg.org/anabasis/yt"
)

func play(s []string, audioOnly bool) error {
	videos, err := linkHandler(s)
	if err != nil {
		return err
	}

	return playVideos(videos, audioOnly)
}

func playVideos(videos []yt.Video, audioOnly bool) error {
	if len(videos) == 0 {
		return fmt.Errorf("no videos provided")
	}

	args := []string{}

	for _, video := range videos {
		args = append(args, "--{")

		maxBitrateVideo := yt.StreamFormat{}
		maxBitrateAudio := yt.StreamFormat{}

		for _, format := range video.StreamingData.AdaptiveFormats {
			if strings.HasPrefix(format.MimeType, "audio") {
				// find maximum bitrate audio
				if format.Bitrate > maxBitrateAudio.Bitrate {
					maxBitrateAudio = format
				}
			} else if !audioOnly && strings.HasPrefix(format.MimeType, "video") {
				// only find maximum bitrate video if audio-only is not specified
				if format.Bitrate > maxBitrateVideo.Bitrate {
					maxBitrateVideo = format
				}
			}
		}

		if !audioOnly {
			args = append(args, maxBitrateVideo.URL, "--audio-file="+maxBitrateAudio.URL)
		} else {
			args = append(args, maxBitrateAudio.URL)
		}

		for _, captions := range video.CaptionData {
			args = append(args, "--sub-file="+captions.BaseURL+"&fmt=vtt")
		}

		args = append(args, "--force-media-title="+video.Title, "--}")
	}

	bin, err := exec.LookPath("mpv")
	if err != nil {
		return err
	}

	cmd := exec.Command(bin, args...)

	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr

	err = cmd.Run()
	if err != nil {
		return err
	}

	return nil
}

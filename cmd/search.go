package main

import (
	"fmt"
	"log"

	"codeberg.org/anabasis/yt"
	"github.com/koki-develop/go-fzf"
)

func search(query string, param yt.SearchParam) ([]yt.Video, error) {
	results, err := yt.GetSearchResults(query, param, "en")
	if err != nil {
		return []yt.Video{}, fmt.Errorf("failed at getting search results: %w", err)
	}

	f, err := fzf.New(fzf.WithNoLimit(true))
	if err != nil {
		return []yt.Video{}, fmt.Errorf("failed at constructing new fuzzy finder: %w", err)
	}

	switch param {
	case yt.SearchParamVideo:
		return displayVideos(f, *results.Videos)
	case yt.SearchParamChannel:
		return displayChannels(f, *results.Channels)
	case yt.SearchParamPlaylist:
		return displayPlaylists(f, *results.Playlists)
	}

	return []yt.Video{}, nil
}

func displayVideos(f *fzf.FZF, items []yt.SimpleVideoSearchResult) ([]yt.Video, error) {
	itemFunc := func(i int) string { return fmt.Sprintf("%s - %s", items[i].Title, items[i].ChannelName) }
	previewFunc := func(i, _, _ int) string {
		return fmt.Sprintf(
			"Title: %s (%s)\nChannel: %s (%s)\nDuration: %s\nDescription: %s\nViews: %d\nPublished: %s\nThumbnail: %s",
			items[i].Title,
			items[i].ID,
			items[i].ChannelName,
			items[i].ChannelID,
			items[i].Duration,
			items[i].ShortDescription,
			items[i].Views,
			items[i].RelativePublishDate,
			items[i].Thumbnails[len(items[i].Thumbnails)-1].URL,
		)
	}

	idxs, err := f.Find(items, itemFunc, fzf.WithPreviewWindow(previewFunc))
	if err != nil {
		return []yt.Video{}, fmt.Errorf("failed at getting selected values: %w", err)
	}

	videos := []yt.Video{}
	for _, i := range idxs {
		video, err := yt.GetVideo(items[i].ID)
		if err != nil {
			log.Printf("failed at getting video: %v\n", err)
			continue
		}

		videos = append(videos, video)
	}

	return videos, nil
}

func displayChannels(f *fzf.FZF, items []yt.SimpleChannel) ([]yt.Video, error) {
	itemFunc := func(i int) string {
		return fmt.Sprintf("%s (%s) - %s", items[i].Name, items[i].Username, items[i].ShortDescription)
	}
	previewFunc := func(i, _, _ int) string {
		return fmt.Sprintf(
			"Name: %s (%s)\nDescription: %s\nVerified: %t\nAvatar: %s",
			items[i].Name,
			items[i].Username,
			items[i].ShortDescription,
			items[i].IsVerified,
			items[i].Avatars[len(items[i].Avatars)-1].URL,
		)
	}

	idxs, err := f.Find(items, itemFunc, fzf.WithPreviewWindow(previewFunc))
	if err != nil {
		return []yt.Video{}, fmt.Errorf("failed at getting selected values: %w", err)
	}

	videos := []yt.Video{}
	for _, i := range idxs {
		channel, err := yt.GetChannel(items[i].ID)
		if err != nil {
			log.Printf("failed at getting channel: %v\n", err)
			continue
		}

		for _, x := range channel.Videos {
			video, err := yt.GetVideo(x.ID)
			if err != nil {
				fmt.Printf("failed at getting video: %v\n", err)
				continue
			}

			videos = append(videos, video)
		}
	}

	return videos, nil
}

func displayPlaylists(f *fzf.FZF, items []yt.SimplePlaylist) ([]yt.Video, error) {
	itemFunc := func(i int) string { return fmt.Sprintf("%s - %s", items[i].Title, items[i].CreatorChannelName) }
	previewFunc := func(i, _, _ int) string {
		return fmt.Sprintf(
			"Title: %s (%s)\nCreator: %s (%s)\nLength: %d videos\nThumbnail: %s",
			items[i].Title,
			items[i].ID,
			items[i].CreatorChannelName,
			items[i].CreatorChannelID,
			items[i].VideoNumber,
			items[i].Thumbnails[len(items[i].Thumbnails)-1].URL,
		)
	}

	idxs, err := f.Find(items, itemFunc, fzf.WithPreviewWindow(previewFunc))
	if err != nil {
		return []yt.Video{}, fmt.Errorf("failed at getting selected values: %w", err)
	}

	videos := []yt.Video{}
	for _, i := range idxs {
		list, err := yt.GetPlaylist(items[i].ID)
		if err != nil {
			log.Printf("failed at getting playlist: %v\n", err)
			continue
		}

		for _, x := range list.Videos {
			video, err := yt.GetVideo(x.ID)
			if err != nil {
				log.Printf("failed at getting video: %v\n", err)
				continue
			}

			videos = append(videos, video)
		}
	}

	return videos, nil
}

func searchMusic(query string, param yt.MusicParam) ([]yt.Video, error) {
	results, err := yt.GetMusicSearchResults(query, param, "en")
	if err != nil {
		return []yt.Video{}, fmt.Errorf("failed at getting search results: %w", err)
	}

	f, err := fzf.New(fzf.WithNoLimit(true))
	if err != nil {
		return []yt.Video{}, fmt.Errorf("failed at constructing new fuzzy finder: %w", err)
	}

	switch param {
	case yt.MusicParamSongs:
		return displaySimpleVideos(f, *results.Songs)
	case yt.MusicParamAlbums:
		return displayAlbums(f, *results.Albums)
	case yt.MusicParamVideos:
		return displaySimpleVideos(f, *results.Videos)
	case yt.MusicParamArtists:
		return displayArtists(f, *results.Artists)
	case yt.MusicParamFeaturedPlaylists:
		return displayPlaylists(f, *results.FeaturedPlaylists)
	case yt.MusicParamCommunityPlaylists:
		return displayPlaylists(f, *results.CommunityPlaylists)
	}

	return []yt.Video{}, nil
}

func displaySimpleVideos(f *fzf.FZF, items []yt.SimpleVideo) ([]yt.Video, error) {
	itemFunc := func(i int) string { return fmt.Sprintf("%s - %s", items[i].Title, items[i].ChannelName) }
	previewFunc := func(i, _, _ int) string {
		return fmt.Sprintf(
			"Title: %s (%s)\nChannel: %s (%s)\nDuration: %v\nThumbnail: %s",
			items[i].Title,
			items[i].ID,
			items[i].ChannelName,
			items[i].ChannelID,
			items[i].Duration,
			items[i].Thumbnails[len(items[i].Thumbnails)-1].URL,
		)
	}

	idxs, err := f.Find(items, itemFunc, fzf.WithPreviewWindow(previewFunc))
	if err != nil {
		return []yt.Video{}, fmt.Errorf("failed at getting selected values: %w", err)
	}

	videos := []yt.Video{}
	for _, i := range idxs {
		video, err := yt.GetVideo(items[i].ID)
		if err != nil {
			log.Printf("failed at getting video: %v\n", err)
			continue
		}

		videos = append(videos, video)
	}

	return videos, nil
}

func displayAlbums(f *fzf.FZF, items []yt.SimpleAlbumPlaylist) ([]yt.Video, error) {
	itemFunc := func(i int) string { return fmt.Sprintf("%s - %s", items[i].Title, items[i].ArtistChannelName) }
	previewFunc := func(i, _, _ int) string {
		return fmt.Sprintf(
			"Title: %s (%s)\nArtist: %s (%s)\nExplicit: %t\nAlbum Type: %s\nReleased: %d\nThumbnail: %s",
			items[i].Title,
			items[i].ID,
			items[i].ArtistChannelName,
			items[i].ArtistChannelID,
			items[i].IsExplicit,
			items[i].AlbumType,
			items[i].ReleaseYear,
			items[i].Thumbnails[len(items[i].Thumbnails)-1].URL,
		)
	}

	idxs, err := f.Find(items, itemFunc, fzf.WithPreviewWindow(previewFunc))
	if err != nil {
		return []yt.Video{}, fmt.Errorf("failed at getting selected values: %w", err)
	}

	videos := []yt.Video{}
	for _, i := range idxs {
		list, err := yt.GetPlaylist(items[i].ID)
		if err != nil {
			log.Printf("failed at getting playlist: %v\n", err)
			continue
		}

		for _, x := range list.Videos {
			video, err := yt.GetVideo(x.ID)
			if err != nil {
				log.Printf("failed at getting video: %v", err)
				continue
			}

			videos = append(videos, video)
		}
	}

	return videos, nil
}

func displayArtists(f *fzf.FZF, items []yt.SimpleArtistChannel) ([]yt.Video, error) {
	itemFunc := func(i int) string {
		return items[i].Name
	}
	previewFunc := func(i, _, _ int) string {
		return fmt.Sprintf(
			"Name: %s (%s)\nSubscribers: %d\nThumbnails: %s",
			items[i].Name,
			items[i].ID,
			items[i].Subscribers,
			items[i].Thumbnails[len(items[i].Thumbnails)-1].URL,
		)
	}

	idxs, err := f.Find(items, itemFunc, fzf.WithPreviewWindow(previewFunc))
	if err != nil {
		return []yt.Video{}, fmt.Errorf("failed at getting selected values: %w", err)
	}

	videos := []yt.Video{}
	for _, i := range idxs {
		channel, err := yt.GetChannel(items[i].ID)
		if err != nil {
			log.Printf("failed at getting channel: %v\n", err)
			continue
		}

		for _, x := range channel.Videos {
			video, err := yt.GetVideo(x.ID)
			if err != nil {
				log.Printf("failed at getting video: %v\n", err)
				continue
			}

			videos = append(videos, video)
		}
	}

	return videos, nil
}

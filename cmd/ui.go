package main

import (
	"io"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/charmbracelet/bubbles/progress"
	tea "github.com/charmbracelet/bubbletea"
)

var prog *tea.Program

const (
	maxWidth = 80
	padding  = 2
)

type progressModel struct {
	updater  *progressUpdater
	progress progress.Model
	err      error
}

func (m progressModel) Init() tea.Cmd {
	return nil
}

func (m progressModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		if msg.String() == "ctrl+c" || msg.String() == "q" {
			return m, tea.Quit
		}

		return m, nil
	case tea.WindowSizeMsg:
		m.progress.Width = msg.Width - padding*2 - 4
		if m.progress.Width > maxWidth {
			m.progress.Width = maxWidth
		}

		return m, nil
	case error:
		m.err = msg

		return m, tea.Quit
	case float64:
		cmds := []tea.Cmd{}
		if msg >= 1.0 {
			cmds = append(cmds, tea.Sequence(
				tea.Tick(time.Millisecond*500, func(time.Time) tea.Msg { return nil }),
				tea.Quit,
			))
		}
		cmds = append(cmds, m.progress.SetPercent(float64(msg)))

		return m, tea.Batch(cmds...)
	case progress.FrameMsg:
		progressModel, cmd := m.progress.Update(msg)
		m.progress = progressModel.(progress.Model)

		return m, cmd
	default:
		return m, nil
	}
}

func (m progressModel) View() string {
	if m.err != nil {
		return "error downloading: " + m.err.Error() + "\n"
	}

	pad := strings.Repeat(" ", padding)

	return "\n" + pad + m.progress.View() + "\n" + pad + m.updater.title + "\n"
}

func addProgressBar(resp *http.Response, file *os.File) {
	updater := &progressUpdater{
		total: int(resp.ContentLength),
		updateBarFunc: func(f float64) {
			prog.Send(f)
		},
		title: file.Name(),
	}

	prog = tea.NewProgram(progressModel{
		updater:  updater,
		progress: progress.New(progress.WithDefaultScaledGradient()),
	})

	go (func() {
		_, err := io.Copy(file, io.TeeReader(resp.Body, updater))
		if err != nil {
			prog.Send(err)
		}
	})()

	prog.Run()
}

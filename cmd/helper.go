package main

import (
	"log"
	"regexp"

	"codeberg.org/anabasis/yt"
)

var (
	domainPattern = `^(?:https?://)?(?:www\.|music\.)?(?:youtube\.com|youtu\.be)`

	videoRegex    = regexp.MustCompile(domainPattern + `/(?:watch\?v=|shorts/|embed/)?`)
	playlistRegex = regexp.MustCompile(domainPattern + `/(?:playlist|embed)\?list=`)
	channelRegex  = regexp.MustCompile(domainPattern + `/channel/`)
)

func linkHandler(s []string) ([]yt.Video, error) {
	var videos []yt.Video

	for _, x := range s {
		switch {
		case videoRegex.MatchString(x):
			vid, err := yt.GetVideo(videoRegex.ReplaceAllString(x, ""))
			if err != nil {
				log.Printf("failed at getting video: %v\n", err)
				continue
			}

			videos = append(videos, vid)
		case playlistRegex.MatchString(x):
			list, err := yt.GetPlaylist(playlistRegex.ReplaceAllString(x, ""))
			if err != nil {
				log.Printf("failed at getting playlist: %v\n", err)
				continue
			}

			for _, x := range list.Videos {
				vid, err := yt.GetVideo(x.ID)
				if err != nil {
					log.Printf("failed at getting video: %v\n", err)
					continue
				}

				videos = append(videos, vid)
			}
		case channelRegex.MatchString(x):
			channel, err := yt.GetChannel(channelRegex.ReplaceAllString(x, ""))
			if err != nil {
				log.Printf("failed at getting video: %v\n", err)
				continue
			}

			for _, x := range channel.Videos {
				vid, err := yt.GetVideo(x.ID)
				if err != nil {
					log.Printf("failed at getting video: %v\n", err)
					continue
				}

				videos = append(videos, vid)
			}
		default:
			log.Printf("format for URL %s not recognized\n", x)
		}
	}

	return videos, nil
}

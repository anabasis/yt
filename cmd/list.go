package main

import (
	"fmt"

	"codeberg.org/anabasis/yt"
	"github.com/koki-develop/go-fzf"
)

func viewList(list string) ([]yt.Video, error) {
	var results []yt.SimpleVideo
	var err error

	switch list {
	case "trending":
		results, err = yt.GetTrendingVideos()
		if err != nil {
			return []yt.Video{}, fmt.Errorf("failed at getting trending videos: %w", err)
		}
	case "home", "front", "homepage", "frontpage":
		results, err = yt.GetFrontPageVideos()
		if err != nil {
			return []yt.Video{}, fmt.Errorf("failed at getting front page videos: %w", err)
		}
	default:
		return []yt.Video{}, fmt.Errorf("invalid list name")
	}

	f, err := fzf.New(fzf.WithNoLimit(true))
	if err != nil {
		return []yt.Video{}, fmt.Errorf("failed at constructing new fuzzy finder: %w", err)
	}

	return displaySimpleVideos(f, results)
}

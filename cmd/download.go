package main

import (
	"fmt"
	"net/http"
	"os"
	"regexp"
	"strings"

	"codeberg.org/anabasis/yt"
	ffmpeg "github.com/u2takey/ffmpeg-go"
)

var (
	fileExtPattern        = regexp.MustCompile(`.*/|;.*`)
	reservedFileNameChars = regexp.MustCompile(`[\0<>:"/\\|?*]`)
)

type progressUpdater struct {
	total         int
	downloaded    int
	updateBarFunc func(float64)
	title         string
}

func (p *progressUpdater) Write(b []byte) (int, error) {
	p.downloaded += len(b)

	if p.total > 0 && p.updateBarFunc != nil {
		p.updateBarFunc(float64(p.downloaded) / float64(p.total))
	}

	return len(b), nil
}

func download(s []string, audioOnly bool) error {
	videos, err := linkHandler(s)
	if err != nil {
		return err
	}

	return downloadVideos(videos, audioOnly)
}

func downloadVideos(videos []yt.Video, audioOnly bool) error {
	for _, video := range videos {
		fileBaseName := reservedFileNameChars.ReplaceAllString(fmt.Sprintf("%s [%s]", video.Title, video.ID), "_")

		maxBitrateVideo := yt.StreamFormat{}
		maxBitrateAudio := yt.StreamFormat{}

		for _, format := range video.StreamingData.AdaptiveFormats {
			if audioOnly && strings.HasPrefix(format.MimeType, "audio") {
				// find maximum bitrate value if audio-only is specified
				if format.Bitrate > maxBitrateAudio.Bitrate {
					maxBitrateAudio = format
				}
			} else if !audioOnly && strings.HasPrefix(format.MimeType, "audio") {
				// use highest bitrate for same format of audio as video (if video value is assigned)
				fileFormat, _, _ := strings.Cut(strings.TrimPrefix(maxBitrateVideo.MimeType, "video"), "; ")

				if strings.HasPrefix(format.MimeType, "audio"+fileFormat) {
					if format.Bitrate > maxBitrateAudio.Bitrate {
						maxBitrateAudio = format
					}
				}
			} else if !audioOnly && strings.HasPrefix(format.MimeType, "video") {
				// only find maximum bitrate video if audio-only is not specified
				if format.Bitrate > maxBitrateVideo.Bitrate {
					maxBitrateVideo = format
				}
			}
		}

		if !audioOnly {
			err := downloadStreamFormat(fileBaseName, maxBitrateAudio)
			if err != nil {
				return err
			}

			err = downloadStreamFormat(fileBaseName, maxBitrateVideo)
			if err != nil {
				return err
			}

			postProcessDownload(
				fileExtPattern.ReplaceAllString(maxBitrateVideo.MimeType, ""),
				fileBaseName,
				video.Title,
				maxBitrateAudio,
				maxBitrateVideo,
			)
		} else {
			err := downloadStreamFormat(fileBaseName, maxBitrateAudio)
			if err != nil {
				return err
			}

			ext := ""
			if strings.HasPrefix(maxBitrateAudio.MimeType, "audio/webm") {
				ext = "opus"
			} else if strings.HasPrefix(maxBitrateAudio.MimeType, "audio/mp4") {
				ext = "m4a"
			}

			postProcessDownload(ext, fileBaseName, video.Title, maxBitrateAudio)
		}
	}

	return nil
}

func downloadStreamFormat(fileBaseName string, format yt.StreamFormat) error {
	client := &http.Client{}

	req, err := http.NewRequest(http.MethodGet, format.URL, nil)
	if err != nil {
		return err
	}

	req.Header.Set("User-Agent", "com.google.android.youtube/18.22.35")
	req.Header.Set("Range", "bytes=0-")

	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	file, err := os.Create(fmt.Sprintf("%s.%d.tmp", fileBaseName, format.ITag))
	if err != nil {
		return err
	}

	defer file.Close()

	addProgressBar(resp, file)

	return nil
}

func postProcessDownload(ext string, fileBaseName string, title string, formats ...yt.StreamFormat) error {
	streams := []*ffmpeg.Stream{}
	for _, format := range formats {
		streams = append(streams, ffmpeg.Input(fmt.Sprintf("%s.%d.tmp", fileBaseName, format.ITag)))
	}

	err := ffmpeg.Output(streams, fileBaseName+"."+ext, ffmpeg.KwArgs{"codec": "copy", "metadata": "title=" + title}).Run()
	if err != nil {
		for _, format := range formats {
			fileExt := ext
			if strings.HasPrefix(format.MimeType, "audio/webm") {
				fileExt = "opus"
			} else if strings.HasPrefix(format.MimeType, "audio/mp4") {
				fileExt = "m4a"
			}

			err = os.Rename(
				fmt.Sprintf("%s.%d.tmp", fileBaseName, format.ITag),
				fmt.Sprintf("%s.%d.%s", fileBaseName, format.ITag, fileExt),
			)
			if err != nil {
				return err
			}
		}

		return nil
	}

	for _, format := range formats {
		err = os.Remove(fmt.Sprintf("%s.%d.tmp", fileBaseName, format.ITag))
		if err != nil {
			return err
		}
	}

	return nil
}

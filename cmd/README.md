# `yt-cli`

[![Go Reference](https://pkg.go.dev/badge/codeberg.org/anabasis/yt/cmd)](https://pkg.go.dev/codeberg.org/anabasis/yt/cmd)

`yt-cli` is a client for accessing YouTube written in Go.
To use it, install by running the following:

```shell
go install codeberg.org/anabasis/yt/cmd
```

## :gear: Features
`yt-cli` is still currently a work-in-progress, but it does implement the following:
- downloading videos
- playing videos
- searching for videos on YouTube and YouTube Music

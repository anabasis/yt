package main

import (
	"errors"
	"log"
	"os"
	"strings"

	"codeberg.org/anabasis/yt"
	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Name:  "yt-cli",
		Usage: "an unofficial YouTube client",
		Commands: []*cli.Command{
			{
				Name:    "play",
				Aliases: []string{"p"},
				Usage:   "Play media with mpv",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:    "audio-only",
						Aliases: []string{"a"},
						Usage:   "play only audio",
					},
				},
				Subcommands: []*cli.Command{wrapSearchCommand(playVideos), wrapListCommand(playVideos)},
				Action: func(c *cli.Context) error {
					if c.Args().Len() > 0 {
						return play(c.Args().Slice(), c.Bool("audio-only"))
					}

					return cli.ShowSubcommandHelp(c)
				},
			},
			{
				Name:    "download",
				Aliases: []string{"d"},
				Usage:   "Download media",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:    "audio-only",
						Aliases: []string{"a"},
						Usage:   "play only audio",
					},
				},
				Subcommands: []*cli.Command{wrapSearchCommand(downloadVideos), wrapListCommand(downloadVideos)},
				Action: func(c *cli.Context) error {
					if c.Args().Len() > 0 {
						return download(c.Args().Slice(), c.Bool("audio-only"))
					}

					return cli.ShowSubcommandHelp(c)
				},
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatalln(err)
	}
}

func wrapSearchCommand(wrapperFunc func([]yt.Video, bool) error) *cli.Command {
	return &cli.Command{
		Name:    "search",
		Aliases: []string{"s"},
		Usage:   "Perform a search",
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    "music",
				Aliases: []string{"m"},
				Usage:   "search YouTube Music",
				Value:   false,
			},
			&cli.StringFlag{
				Name:    "type",
				Aliases: []string{"t"},
				Usage:   "specify search type",
				Value:   "video",
				Action: func(c *cli.Context, s string) error {
					s = strings.TrimSuffix(strings.ToLower(s), "s")

					if c.Bool("music") &&
						(s == "song" || s == "album" || s == "video" || s == "artist" ||
							s == "featured playlist" || s == "community playlist") {
						return nil
					} else if !c.Bool("music") && (s == "video" || s == "channel" || s == "playlist") {
						return nil
					}

					return errors.New("type not found")
				},
			},
		},
		Action: func(c *cli.Context) error {
			if c.Args().Len() > 0 {
				if c.Bool("music") {
					var param yt.MusicParam
					switch strings.TrimSuffix(strings.ToLower(c.String("type")), "s") {
					case "song":
						param = yt.MusicParamSongs
					case "album":
						param = yt.MusicParamAlbums
					case "video":
						param = yt.MusicParamVideos
					case "artist":
						param = yt.MusicParamArtists
					case "featured playlist":
						param = yt.MusicParamFeaturedPlaylists
					case "community playlist":
						param = yt.MusicParamCommunityPlaylists
					default:
						return errors.New("type not found")
					}

					sel, err := searchMusic(strings.Join(c.Args().Slice(), " "), param)
					if err != nil {
						return err
					}

					return wrapperFunc(sel, c.Bool("audio-only"))
				}

				var param yt.SearchParam
				switch strings.TrimSuffix(strings.ToLower(c.String("type")), "s") {
				case "video":
					param = yt.SearchParamVideo
				case "channel":
					param = yt.SearchParamChannel
				case "playlist":
					param = yt.SearchParamPlaylist
				default:
					return errors.New("type not found")
				}

				sel, err := search(strings.Join(c.Args().Slice(), " "), param)
				if err != nil {
					return err
				}

				return wrapperFunc(sel, c.Bool("audio-only"))
			}

			return cli.ShowSubcommandHelp(c)
		},
	}
}

func wrapListCommand(wrapperFunc func([]yt.Video, bool) error) *cli.Command {
	return &cli.Command{
		Name:    "list",
		Aliases: []string{"l"},
		Usage:   "View a list",
		Action: func(c *cli.Context) error {
			if c.Args().Len() > 0 {
				sel, err := viewList(c.Args().First())
				if err != nil {
					return err
				}

				return wrapperFunc(sel, c.Bool("audio-only"))
			}

			return cli.ShowSubcommandHelp(c)
		},
	}
}

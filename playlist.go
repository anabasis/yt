/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package yt

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/tidwall/gjson"
)

// SimplePlaylist is a representation of the data retrieved from search results
// for a playlist on YouTube.
type SimplePlaylist struct {
	ID          string
	Title       string
	VideoNumber int

	CreatorChannelID   string
	CreatorChannelName string

	Thumbnails []Thumbnail
}

// SimpleAlbumPlaylist is a representation of the data retrieved from search
// results for an album on YouTube Music.
type SimpleAlbumPlaylist struct {
	ID    string
	Title string

	IsExplicit  bool
	ReleaseYear int
	AlbumType   string

	ArtistChannelID   string
	ArtistChannelName string

	Thumbnails []Thumbnail
}

// Playlist is a representation of the more complete data for a playlist
// retrieved from the playlist page on YouTube.
type Playlist struct {
	SimplePlaylist

	Views          int
	Description    string
	LastUpdateDate time.Time

	Videos []SimpleVideo
}

// BrowseQuery is the data sent to the YouTube browse endpoint
// (https://www.youtube.com/youtubei/v1/browse) after being JSON-encoded.
type BrowseQuery struct {
	BrowseID string       `json:"browseId"`
	Context  QueryContext `json:"context"`
	Param    Param        `json:"params,omitempty"`
}

// GetPlaylist returns data for a playlist ID as Playlist struct with any
// errors encountered.
func GetPlaylist(id string) (Playlist, error) {
	body, err := sendBrowseRequest("VL"+id, "")
	if err != nil {
		return Playlist{}, err
	}

	data := gjson.ParseBytes(body)

	header := data.Get("header.playlistHeaderRenderer")
	details := data.Get(
		"contents.twoColumnBrowseResultsRenderer.tabs.0.tabRenderer.content.sectionListRenderer.contents.0.itemSectionRenderer.contents.0.playlistVideoListRenderer",
	)

	return Playlist{
		SimplePlaylist: SimplePlaylist{
			ID:          header.Get("playlistId").Str,
			Title:       header.Get("title.simpleText").Str,
			VideoNumber: int(header.Get("numVideosText.runs.0.text").Int()),

			CreatorChannelID:   header.Get("ownerText.runs.0.navigationEndpoint.browseEndpoint.browseId").Str,
			CreatorChannelName: header.Get("ownerText.runs.0.text").Str,

			Thumbnails: thumbnailSlice(
				header.Get("playlistHeaderRender.heroPlaylistThumbnailRenderer.thumnails.thumbnails").Array(),
			),
		},

		Views:          getViewsInt(header.Get("byline.1.playlistBylineRenderer.text.simpleText")),
		Description:    header.Get("descriptionText.simpleText").Str,
		LastUpdateDate: getUpdateDate(header.Get("byline.2.playlistBylineRenderer.text.runs.1.text")),

		Videos: simpleVideoParser([]SimpleVideo{}, details.Get("contents.#.playlistVideoRenderer").Array()),
	}, nil
}

// sendBrowseRequest sends a request to the YouTube browse endpoint
// (https://www.youtube.com/youtubei/v1/browse) with the supplied ID and
// parameter and returns the body of the response and any errors encountered.
func sendBrowseRequest(id string, param Param) ([]byte, error) {
	client := &http.Client{}

	query, err := constructBrowseQuery(id, param)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(
		http.MethodPost,
		"https://www.youtube.com/youtubei/v1/browse?prettyPrint=false",
		bytes.NewReader(query),
	)
	if err != nil {
		return nil, fmt.Errorf("failed at constructing request: %w", err)
	}

	req.Header.Set("User-Agent", webUserAgent)

	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed at performing browse request: %w", err)
	}

	if resp.StatusCode != 200 {
		return nil, errStatusCodeNotOK
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed at reading response of browse request: %w", err)
	}

	return body, nil
}

// constructBrowseQuery returns a JSON-encoded query for the YouTube browse
// endpoint (https://www.youtube.com/youtubei/v1/browse) with the supplied ID
// and parameter as well as any errors encountered.
func constructBrowseQuery(id string, param Param) ([]byte, error) {
	constructedQuery := BrowseQuery{
		BrowseID: id,
		Context: QueryContext{
			ClientContext{
				ClientName:    "WEB",
				ClientVersion: webClientVersion,
				Language:      "en",
			},
		},
	}

	if param != "" {
		constructedQuery.Param = param
	}

	query, err := json.Marshal(constructedQuery)
	if err != nil {
		return nil, fmt.Errorf("failed at encoding JSON value of browse query: %w", err)
	}

	return query, nil
}
